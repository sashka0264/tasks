const {solution, testcases} = require('./task11');

describe('task11', () => {
  test('checking for palindrome', () => {
    testcases.forEach(({ str, result }) => {
      try {
        expect(solution(str)).toEqual(result);
      } catch (e) {
        console.warn({str, result});
        throw e;
      }
    });
  })
})
