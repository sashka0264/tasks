/**
 Необходимо написать функцию, которая проверяет, является ли строка палиндромом. 
 Возвращает true или false в зависимости от условий.
**/

function palindrome(str) {}

const testcases = [
  {
    str: '1abcdedcba1',
    result: true
  },
  {
    str: 'abScded',
    result: false
  }
];

module.exports['testcases'] = testcases;
module.exports['solution'] = palindrome;